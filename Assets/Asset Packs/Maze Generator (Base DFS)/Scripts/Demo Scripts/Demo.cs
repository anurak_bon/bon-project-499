﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
//This is just a demo script to show how this system works

public class Demo : MonoBehaviour {

    public NavMeshSurface surface;
    public GameObject ruleManager;

    public Slider xSize;
	public Slider ySize;
	public Slider cam;

    // Show when edit x*y on slider
    public Text xSizeText;
    public Text ySizeText;

    // Use this for initialization
    void Start () {
        ShowValueSizeXY();
        
    }
	
	// Update is called once per frame
	void Update () {
		if (cam)
		    transform.position = new Vector3 (transform.position.x, cam.value*2.5f, transform.position.z);        
    }

	void Create()
    {
        Maze myScript = transform.GetComponent<Maze>();
        if (xSize && ySize && myScript)
        {
            myScript.xSize = (int)xSize.value;
            myScript.ySize = (int)ySize.value;
            myScript.Generate();
        }

        ReMazeSize();

        // Start My Game 
        ruleManager.SetActive(true);
        surface.BuildNavMesh();
    }

  

    void Reset (){
        LoadCurrentScene();
	}

    public void ShowValueSizeXY()
    {
        xSizeText.text = xSize.value.ToString();
        ySizeText.text = ySize.value.ToString();
    }

    public int GetMazeSizeX()
    {
        return (int)xSize.value;
    }

    public int GetMazeSizeY()
    {
        return (int)ySize.value;
    }

    private static void ReMazeSize()
    {
        GameObject maze = GameObject.Find("Maze");
        maze.transform.localScale += new Vector3(2f, 2f, 2f);
    }

    private void LoadCurrentScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
}
