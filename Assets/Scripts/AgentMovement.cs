﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentMovement : MonoBehaviour {

    NavMeshAgent nav;

    // Use this for initialization
    void Start () {
        nav = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        if (FindClosestEnemy() != null)
        {
            nav.SetDestination(FindClosestEnemy().transform.position);
        }
    }

    private GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Item");
        GameObject closest = null;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            closest = go;
        }
        return closest;
    }
}
