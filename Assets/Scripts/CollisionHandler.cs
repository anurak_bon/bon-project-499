﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // ok as long as this is the only script that loads scenes

public class CollisionHandler : MonoBehaviour
{
    Item item;
    CollectionGoal collectionGoal;

    [Tooltip("In seconds")] [SerializeField] float levelLoadDelay = 1.5f;
    [Tooltip("FX prefab on player")] [SerializeField] GameObject deathFX;


    private void Start()
    {
        
    }

    // How to know for use OnCollisionEnter or OnTriggerEnter? 
    // Answer: Look at table of when you checked Trigger in collider or checked Kinetic in rigid body
    // ref.https://docs.unity3d.com/Manual/CollidersOverview.html
    void OnTriggerEnter(Collider other)
    {
        // StartDeathSequence();
        // deathFX.SetActive(true);
        // Invoke("ReloadScene", levelLoadDelay);

        if (other.gameObject.CompareTag("Item"))
        {
            //Invoke("StartSuccessSequence", levelLoadDelay);            
        }

    }  

    private void StartDeathSequence()
    {
        print("Player dying");
        SendMessage("OnPlayerDeath");
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(1);
    }

    private void StartSuccessSequence()
    {
        Invoke("LoadNextLevel", levelLoadDelay);
    }

    private void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0; // loop back to start
        }
        SceneManager.LoadScene(nextSceneIndex);
    }
}
