﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject deathFX;
    [SerializeField] Transform parent;

    // Use this for initialization
    void Start()
    {
        // Adding A Component From Script
        // gameObject.AddComponent<TypeToAdd>();
        // Ex. Collider someCollider = gameObject.AddComponent<SphereCollider>();

        AddNonTriggerSphereCollider();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    void OnParticleCollision(GameObject other)
    {
        GameObject fx = Instantiate(deathFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        // print("Particles collided with enemy " + gameObject.name);
        Destroy(gameObject);
    }

    private void AddNonTriggerSphereCollider()
    {
        Collider SphereCollider = gameObject.AddComponent<SphereCollider>();
        SphereCollider.isTrigger = false;
    }
}
