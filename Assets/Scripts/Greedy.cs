﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Greedy : MonoBehaviour
{

    // [SerializeField] private Transform target;
    NavMeshAgent nav;

    private void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start()
    {
        //nav.Warp(transform.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (FindClosestEnemy() != null)
        {
        
            nav.SetDestination(FindClosestEnemy().transform.position);
           

            //nav.Warp(FindClosestEnemy().transform.position);
            //nav.SetDestination(FindClosestEnemy().transform.position);
            //Vector3 target = FindClosestEnemy().transform.position;
            //transform.position = Vector3.MoveTowards(transform.position, target, 10f * Time.deltaTime);
        }
    }

    public GameObject FindClosestEnemy()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Item");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                // distance = curDistance;
            }
        }
        return closest;
    }
}
