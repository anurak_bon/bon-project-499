﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private string type;

    [SerializeField] GameObject deathFX;
    [SerializeField] Transform parent;

    [SerializeField] int scorePerHit = 12;

    ScoreBoard scoreBoard;
    ScoreBoardAgent scoreBoardAgent;

    CollectionGoal collectionGoal;
    CollectionGoalBot collectionGoalBot;

    // Use this for initialization
    void Start()
    {
        // Adding A Component From Script
        // gameObject.AddComponent<TypeToAdd>();
        // Ex. Collider someCollider = gameObject.AddComponent<BoxCollider>();
        AddTriggerBoxCollider();

        //assign score board object
        scoreBoard = FindObjectOfType<ScoreBoard>();
        scoreBoardAgent = FindObjectOfType<ScoreBoardAgent>();

        collectionGoal = FindObjectOfType<CollectionGoal>();
        collectionGoalBot = FindObjectOfType<CollectionGoalBot>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            scoreBoard.ScoreHit(scorePerHit);
            collectionGoal.ItemPickedUp(this, other);
        }
        else if (other.CompareTag("Agent"))
        {
            scoreBoardAgent.ScoreHit(scorePerHit);
            collectionGoalBot.ItemPickedUp(this, other);
        }

        if (!other.CompareTag("CellFloor"))
            DestroyItem();
    }

    private void DestroyItem()
    {
        GameObject fx = Instantiate(deathFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        //print("Trigger collided with item: " + gameObject.name);
        Destroy(gameObject);
    }

    private void AddTriggerBoxCollider()
    {
        Collider BoxCollider = gameObject.AddComponent<BoxCollider>();
        BoxCollider.isTrigger = true;
    }

    public string GetItemType()
    {
        return type;
    }


}
