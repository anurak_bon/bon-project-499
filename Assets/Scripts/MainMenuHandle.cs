﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class MainMenuHandle : MonoBehaviour
{

    [SerializeField] private Button playModeButon;
    [SerializeField] private Button showWorkingOfBotButton;
    [SerializeField] private Button exitGameButton;

    SceneLoader sceneLoader;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    public void ExitGameOnClick()
    {
        Application.Quit();
    }



    private void ShowWorkingBotOnClick()
    {
        print("ShowWorkingBotOnClick");
    }

    public void PlayGame()
    {
        LoadNextScene();
    }

    public void LoadCurrentScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0; // loop back to start
        }
        SceneManager.LoadScene(nextSceneIndex);
    }

}
