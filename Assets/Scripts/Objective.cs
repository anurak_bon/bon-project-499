﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objective : MonoBehaviour
{
    Text objectiveText;
    RuleManager ruleManager;




    // Use this for initialization
    void Start()
    {
        //ruleManager = FindObjectOfType<RuleManager>();
        objectiveText = GetComponent<Text>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    public void SetQuest(string desc)
    {
        objectiveText.text = ruleManager.GetObjective().ToString();
    }

}
