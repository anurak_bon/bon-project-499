﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{

    [Header("General")]
    [Tooltip("In ms^-1")]
    [SerializeField]
    float controlSpeed = 20f;
    [Tooltip("In m")] [SerializeField] float xRange = 6f;
    [Tooltip("In m")] [SerializeField] float yRange = 4.5f;

    [Header("Screen-position Based")]
    [SerializeField]
    float positionPitchFactor = -5f;
    [SerializeField] float positionYawFactor = -5f;

    [Header("Control-throw Based")]
    [SerializeField]
    float controlPitchFactor = -20f;
    [SerializeField] float controlRollFactor = -30f;

    float xThrow, yThrow;
    bool isControlEnabled = true;


    // Animation
    private Animator anim;
    public float animSpeed = 1.5f;
    private AnimatorStateInfo currentBaseState;
    private Rigidbody rb;

    public float jumpPower = 10f;

    static int idleState = Animator.StringToHash("Base Layer.Idle");
	static int locoState = Animator.StringToHash("Base Layer.Locomotion");
	static int jumpState = Animator.StringToHash("Base Layer.Jump");
	static int restState = Animator.StringToHash("Base Layer.Rest");

    // Use this for initialization
    void Awake()
    {
        // transform.position = new Vector3(0f, -1.6f, 7.1f);

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
  
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ProcessRotation();
        ProcessTranslation();
        // ProcessAnimation();
    }

    private void ProcessAnimation()
    {
        anim.speed = animSpeed;
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);

        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
            anim.SetBool("Jump", true);     // Animatorにジャンプに切り替えるフラグを送る
        }

        if(Input.GetButtonDown("Horizontal"))
        {
            anim.SetBool("Locomotion", true);     // Animatorにジャンプに切り替えるフラグを送る
        }
    }

    private void ProcessTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");

        float xOffset = xThrow * controlSpeed * Time.deltaTime;
        float yOffset = yThrow * controlSpeed * Time.deltaTime;

        float rawXPos = transform.position.x + xOffset;
        float rawYPos = transform.position.z + yOffset;
        //transform.position = new Vector3(rawXPos, transform.position.y, rawYPos);

        Vector3 movement = new Vector3(rawXPos, transform.position.y, rawYPos);

        // Apply this movement to the rigidbody's position.
        rb.MovePosition(movement);
    }

    private void ProcessRotation()
    {
        float moveHorizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float moveVertical = CrossPlatformInputManager.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        //transform.rotation = Quaternion.LookRotation(movement);
        rb.MoveRotation(Quaternion.LookRotation(movement));

    }
}
