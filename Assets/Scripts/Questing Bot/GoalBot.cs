﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalBot : MonoBehaviour
{
    [SerializeField] public QuestBot Quest { get; set; }
    [SerializeField] public string Description { get; set; }
    [SerializeField] public bool Completed { get; set; }
    [SerializeField] public int CurrentAmount { get; set; }
    [SerializeField] public int RequiredAmount { get; set; }



    public virtual void Init()
    {
        // default init stuff
    }

    public void Evaluate()
    {
        if (CurrentAmount >= RequiredAmount)
        {
            Complete();
        }
        print("CurrentAmount: " + CurrentAmount.ToString());
        print("RequiredAmount: " + RequiredAmount.ToString());

        //currAmountText.text = CurrentAmount.ToString();
        //requirAmountText.text = RequiredAmount.ToString();

    }

    public void Complete()
    {
        Quest.CheckGoals();
        Completed = true;
        Debug.Log("Goal marked as completed.");
        //print("Completed= " + Completed.ToString());
    }

    public int GetCurrentAmount()
    {
        return CurrentAmount;
    }

    public int GetRequiredAmount()
    {
        return RequiredAmount;
    }

}
