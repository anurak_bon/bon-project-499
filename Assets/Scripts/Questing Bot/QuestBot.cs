﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class QuestBot : MonoBehaviour
{
    private int level;
    public int Level
    {
        get
        {
            return level;
        }
        set
        {
            level = value;
        }
    }

    public List<GoalBot> Goals { get; set; }
    public string QuestName { get; set; }
    public string Description { get; set; }
    public int ExperienceReward { get; set; }
    public Item ItemReward { get; set; }
    public bool Completed { get; set; }

    public void CheckGoals()
    {
        Completed = Goals.All(g => g.Completed);
        //for (int i = 1; i < Goals.Count; i++)
        //{
        //    Goals[i].Completed = true;
        //}
        if (Completed)
        {
            GiveReward();
        }
    }

    public void GiveReward()
    {
        print("ItemReward");
        if (ItemReward != null)
        {
            print("Inner If ItemReward");
            // InventoryController.Instance.GiveItem(ItemReward);
        }

    }

    public bool IsCompleteQuest()
    {
        return Completed;
    }
}
