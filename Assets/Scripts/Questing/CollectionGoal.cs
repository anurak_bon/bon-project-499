﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionGoal : Goal1
{
    Item item;


    public string ItemID { get; set; }

    public CollectionGoal(Quest quest, string itemID, string description, bool completed, int currentAmount, int requiredAmount)
    {
        this.Quest = quest;
        this.ItemID = itemID;
        this.Description = description;
        this.Completed = completed;
        this.CurrentAmount = currentAmount;
        this.RequiredAmount = requiredAmount;
    }

    public void InitialValue(Quest quest, string itemID, string description, bool completed, int currentAmount, int requiredAmount)
    {
        this.Quest = quest;
        this.ItemID = itemID;
        this.Description = description;
        this.Completed = completed;
        this.CurrentAmount = currentAmount;
        this.RequiredAmount = requiredAmount;
    }

    public CollectionGoal SetCollectionValue(Quest quest, string itemID, string description, bool completed, int currentAmount, int requiredAmount)
    {
        this.Quest = quest;
        this.ItemID = itemID;
        this.Description = description;
        this.Completed = completed;
        this.CurrentAmount = currentAmount;
        this.RequiredAmount = requiredAmount;
        return this;
    }

    public override void Init()
    {
        base.Init();
    }

    public void ItemPickedUp(Item item, Collider collier)
    {
        print("this.ItemID:" + this.ItemID);
        if (item.GetItemType() == this.ItemID)
        {
            Debug.Log(this.ItemID);
            Debug.Log("Detected item: " + item.name);
            this.CurrentAmount++;
            Evaluate();
        }
    }

  
}
