﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal1 : MonoBehaviour
{
    [SerializeField] public Quest Quest { get; set; }
    [SerializeField] public string Description { get; set; }
    [SerializeField] public bool Completed { get; set; }
    [SerializeField] public int CurrentAmount { get; set; }
    [SerializeField] public int RequiredAmount { get; set; }

    public virtual void Init()
    {
        // default init stuff
    }

    public void Evaluate()
    {
        if (CurrentAmount >= RequiredAmount)
        {
            Complete();
            print("CurrentAmount: " + CurrentAmount.ToString());
            print("RequiredAmount: " + RequiredAmount.ToString());
        }
    }

    public void Complete()
    {
        Quest.CheckGoals();
        Completed = true;
        Debug.Log("Goal marked as completed.");
        //print("Completed= " + Completed.ToString());
    }

    public int GetCurrentAmount()
    {
        return CurrentAmount;
    }

    public int GetRequiredAmount()
    {
        return RequiredAmount;
    }

}
