﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UltimateSlayer : Quest
{
    [SerializeField] public string questName = "Ultimate Slayer";
    [SerializeField] public string description = "Collect $ = 5";

    [SerializeField] public string itemType = "money";
    [SerializeField] public bool complete = false;
    [SerializeField] public int currentAmount = 0;
    [SerializeField] public int requiredAmount = 5;

    Objective ruleManager;

    CollectionGoal cg;

    Text questDescription;

    //GUI Setting value
    public Slider itemTargetSlider;
    public Text itemTargetSliderText;

    //Show score and amount
    private Goal1 goal;
    [SerializeField] private Text playerCurrentAmount;

    bool isSetGoal = false;

    void Awake()
    {
        //questDescription = FindObjectOfType<Text>();
        //questDescription.text = description;
        Debug.Log("Ultimate slayer assigned.");

        // ItemReward = ItemDatabase.Instance.GetItem("potion_log");
        ExperienceReward = 100;
        //cg = new CollectionGoal(this, "money", "Find 3 Moneys", false, 0, 3);

        SetGoal();
    }

    private void Start()
    {
        //ruleManager = FindObjectOfType<Objective>();
        //ruleManager.SetQuest(description);
        goal = FindObjectOfType<Goal1>();
    }

    private void Update()
    {
        SetCurrentAmount();
        SetRequiredAmount();
    }

    public void SetRequiredAmount()
    {
        requiredAmount = (int)itemTargetSlider.value;
        itemTargetSliderText.text = "Goal Items: " + requiredAmount.ToString();
    }

    private void SetCurrentAmount()
    {
        int newCurrentAmount = goal.GetCurrentAmount();
        int objectiveRequirAmount = goal.GetRequiredAmount();
        playerCurrentAmount.text = "You have " + newCurrentAmount.ToString() + " /" + objectiveRequirAmount.ToString();
    }


    public void SetGoal()
    {
        isSetGoal = true;

        GameObject gameObject = new GameObject("CollectionGoal Object");
        cg = gameObject.AddComponent<CollectionGoal>();
        //cg.InitialValue(this, "money", "Find 3 Moneys", false, 0, 1);


        Goals = new List<Goal1>
        {
           //cg.SetCollectionValue(this, "money", "Find 3 Moneys", false, 0, 2),
           cg.SetCollectionValue(this, itemType, description, complete, currentAmount, requiredAmount)
        };

        //List<Goal1> Goals = new List<Goal1>();
        //Goals.Add(cg.SetCollectionValue(this, "money", "Find 3 Moneys", false, 0, 2));
        //Goals.Add(cg.SetCollectionValue(this, "money", "Find 3 Moneys", false, 0, 2));

        Goals.ForEach(g => g.Init());
    }
}
