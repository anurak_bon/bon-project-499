﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.AI;

public class RuleManager : MonoBehaviour
{
    //public TMP_InputField scoreTargetInput;
    //public TMP_InputField itemTargetInput;

    public InputField scoreTargetInput;
    public InputField itemTargetInput;

    public Slider scoreTargetSlide;
    public Text scoreSliderText;
    //public Slider itemTargetSlider;
    //public Text itemTargetSliderText;

    [SerializeField] private int goalScore = 60;

    ScoreBoard scoreBoard;
    ScoreBoardAgent scoreBoardAgent;

    // GUI
    [SerializeField] private GameObject objectiveUI;
    [SerializeField] private GameObject completeGameUI;
    [SerializeField] private Text winnerText;

    // declare Objective
    [SerializeField] private Quest quest;
    [SerializeField] private QuestBot questBot;

    // A*
    [SerializeField] private GameObject aStarObject;

    // For Spawn character
    [SerializeField] private GameObject agent;
    [SerializeField] private GameObject player;

    private SpawnObject spawnObject;

    string winner;
    Text objectiveText;

    private void Awake()
    {
        
    }
    // Use this for initialization
    void Start()
    {        
        SetGoalScoreSlider();

        OnApplicationPause(true);
        objectiveUI.SetActive(true);
        completeGameUI.SetActive(false);

        //assign score board object
        scoreBoard = FindObjectOfType<ScoreBoard>();
        scoreBoardAgent = FindObjectOfType<ScoreBoardAgent>();

        spawnObject = FindObjectOfType<SpawnObject>();
    }

    public void InitValue()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckWhoCompleteObjective();
    }

    private void CheckWhoCompleteObjective()
    {
        int playerScore = scoreBoard.GetScore();
        int botScore = scoreBoardAgent.GetScore();

        if (quest.IsCompleteQuest() && playerScore >= goalScore)
        {
            winner = "YOU";
            print("WINNER is " + winner);
            ProcessCompleteGame(winner);
        }

        if (questBot.IsCompleteQuest() && botScore >= goalScore)
        {
            winner = "AGENT";
            print("WINNER is " + winner);
            ProcessCompleteGame(winner);
        }
        
    }

    public Text GetObjective()
    {
        return objectiveText;
    }

    private void ProcessCompleteGame(string winner)
    {
        OnApplicationPause(true);
        completeGameUI.SetActive(true);
        winnerText.text = winner + " is Winner";

    }

    public void RestartGame()
    {
        LoadCurrentScene();
    }

    public void LoadNextLevel()
    {
        LoadNextScene();
    }

    public void BackToMainMenu()
    {
        LoadFirstScene();
    }

    // Scene Loader
    private void LoadCurrentScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    private void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0; // loop back to start
        }
        SceneManager.LoadScene(nextSceneIndex);
    }

    private void LoadFirstScene()
    {
        int firstSceneIndex = 0;
        SceneManager.LoadScene(firstSceneIndex);
    }

    public void OnApplicationPause(bool isPaused)
    {
        if (isPaused)
        {
            Time.timeScale = 0;
        }
        if (!isPaused)
        {
            Time.timeScale = 1;
        }
    }
    
    public void PlayOnClick()
    {
        print("game start");
        objectiveUI.SetActive(false);
        OnApplicationPause(false);

        spawnObject.SpawnItemInMaze();
        SpawnPlayer();
        SpawnAgent();
        
        // Start bot
        Invoke("StartAStarSearch", 1.5f);
    }

    private void SpawnPlayer()
    {
        Vector3 pos = new Vector3(0, 0, 0);
        Instantiate(player, pos, Quaternion.identity);
    }

    private void SpawnAgent()
    {
        Vector3 pos = new Vector3(0, 0, 0);
        Instantiate(agent, pos, Quaternion.identity);
    }

    private void StartAStarSearch()
    {
        
        agent.SetActive(true);
        aStarObject.SetActive(true);
    }    

    public void SetGoalScore(string newTargetScore)
    {
        print("Goalscore " + goalScore.ToString());
    }

    public void SetGoalScoreSlider()
    {
        goalScore = (int)scoreTargetSlide.value;
        scoreSliderText.text = "Goal Score: " + goalScore.ToString();
        print("Goalscore " + goalScore.ToString());
    }
}
