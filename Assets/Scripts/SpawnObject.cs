﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{

    [SerializeField] private GameObject ItemPrefab;
    private Demo mazeManager;

    [SerializeField] private Vector3 center;
    [SerializeField] private Vector3 size;

    // Use this for initialization
    void Start()
    {
        mazeManager = FindObjectOfType<Demo>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            SpawnItemInMaze();
        }
    }

    public void SpawnItemInMaze()
    {
        int widthMazeSize = mazeManager.GetMazeSizeX();
        int heightMazeSize = mazeManager.GetMazeSizeY();

        for(int x=0; x<= widthMazeSize; x += 2)
        {
            for (int y = 0; y <= heightMazeSize; y+= 2)
            {
                if(Random.value > 0.4f)
                {
                    // Spawn a item
                    //Vector3 pos = new Vector3(x-widthMazeSize/2f, 1f,y-heightMazeSize/2f);
                    Vector3 pos = new Vector3(Random.Range(-heightMazeSize*1.3f, heightMazeSize * 1.3f), 1, Random.Range(-widthMazeSize * 1.3f, widthMazeSize * 1.3f));
                    //Instantiate(ItemPrefab, pos, Quaternion.Euler(90, 0, 0), transform);
                    Instantiate(ItemPrefab, pos, Quaternion.identity, transform);
                }
            }
        }

    }

    public void SpawnItemEachCell(Vector3 pos)
    {
        Vector3 position = new Vector3(Random.Range(-15.0f, 15.0f), 0, Random.Range(-10.0f, 10.0f));
        Instantiate(ItemPrefab, position, Quaternion.identity);

        //Instantiate(ItemPrefab, pos, Quaternion.identity);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center, size);
    }
}
